# Activité Git et les lignes de commandes

## Activité de recherche d'informations

- Quelle est l'utilité de Git ?
- Quelles sont les alternatives à Git ?
- Trouver l'utilité des commandes Git suivantes :
```
git init
git clone
git add
git commit
git status
git reset
git diff
git log
git branch
git checkout
git remote
git push
git pull
git stash
git show
```

## Activité utiliser Git

Pour pouvoir utiliser le logiciel Git, il faut d'abord l'installer.

- [ ] Ouvrir un terminal. Toutes les instuctions qui vont suivre sont à taper dans le terminal

- [ ] Installer Git en utilisant la commande suivante :
```
sudo apt-get install git
```

- [ ] Vérifier que Git est bien installé en regardant la version installée à l'aide de la commande suivante :
```bash
git --version
```

- [ ] Mettre à jour la configuration de Git (adresse email et nom) à l'aide de la commande suivante (à adapter à votre cas) :
```
git config --global user.email "Vous@exemple.com"
git config --global user.name "Votre Nom"
```

- [ ] Se placer dans le dossier personnel grâce à la commande suivante :
```
cd ~
```

- [ ] Vérifier que vous êtes dans le bon dossier avec la commande :
```
pwd
```

- [ ] Afficher le contenu du dossier avec la commande :
```
ls
```

- [ ] Créer un dossier pour cette activité nommé "activite_git" avec la commande :
```
mkdir activite_git
``` 

- [ ] Vérifier la création du dossier à l'aide d'une commande déjà **vue précédemment**

- [ ] Se déplacer dans le dossier activite_git avec la commande suivante :
```
cd activité_git
```

- [ ] Vérifier l'emplacement du dossier et son contenu grâce à des commandes **vues précédemment**

- [ ] Initialiser un repository Git à l'aide de la commande suivante :
```
git init
```

- [ ] Vérifier qu'un dossier caché nommé .git a bien été créé à l'aide d'une commande déjà **vue précédemment**

- [ ] Vérifier l'état du repository git avec la commande suivante :
```
git status
```
Le retour doit correspondre à ceci :
```
Sur la branche master

Aucun commit

rien à valider (créez/copiez des fichiers et utilisez "git add" pour les suivre)
```

- [ ] Créer un fichier nommé "README.md" à l'aide de la commande suivante :
```
touch README.md
```

- [ ] Vérifier que le fichier a bien été créé à l'aide d'une commande déjà **vue précédemment**

- [ ] Ouvrir le fichier et ajouter le texte "Premier repository Git" à l'aide du logiciel nano en tapant la commande :
```
nano README.md
```

- [ ] Vérifier le contenu du fichier README.md à l'aide de la commande suivante :
```
cat README.md
```

- [ ] Vérifier que Git a bien détecté la création de ce fichier à l'aide d'une commande déjà **vue précédemment**

- [ ] Ajouter le fichier README.md à l'index de Git pour le prochain commit avec la commande suivante :
```
git add README.md
```

- [ ] Vérifier que Git a bien effectué l'action précédente à l'aide d'une commande déjà **vue précédemment**

- [ ] Commiter les modifications à l'aide de la commande suivante :
```
git commit -m "Ajout du fichier README.md"
```

- [ ] Vérifier l'état du repository git à l'aide d'une commande déjà **vue précédemment**

- [ ] Regarder l'historique des commits à l'aide de la commande suivante :
```
git log
```

- [ ] Créer un nouveau dossier avec un fichier markdown à l'intérieur contenant du texte, commiter les modifications et vérifier l'historique à l'aide des commandes déjà **vues précédemment**

## Activité faire le lien entre Git et Gitlab

Nous allons maintenant lier le repository Git en local à un repository distant de Gitlab.

- [ ] Créer un nouveau projet sur Gitlab en allant dans *Projects* > *New project* > *Create blank project*, remplir le nom du projet avec "Projet_git" et cliquer sur *Create project*

- [ ] Revenir dans le terminal à l'endroit du repository Git

- [ ] Ajouter le lien vers le repository distant de Gitlab avec la commande suivante (en remplissant votre nom d'utilisateur) :
```
git remote add origin git@gitlab.com:<<votre_nom_utilisateur>>/projet_git.git
```

- [ ] Envoyer les modifications que vous avez effectuées en local (ajouts de fichier, création d'un dossier) vers Gitlab à l'aide de la commande :
```
git push origin main
```

- [ ] Vérifier sur le repository projet_git de Gitlab que le contenu est identique à ce que vous avez sur votre ordinateur

- [ ] Supprimer le dossier "activite_git" qui contient le repository git sur votre ordinateur à l'aide de la commande suivante :
```
sudo rm -r activite_git 
```

- [ ] Créer un dossier "from_gitlab" et se positionner à l'intérieur à l'aide des commandes déjà **vues précédemment**

- [ ] Cloner le repository de Gitlab nommé "projet_git" dans ce dossier à l'aide de la commande (en remplissant votre nom d'utilisateur) :
```
git clone git@gitlab.com:<<votre_nom_utilisateur>>/projet_git.git
```

- [ ] Vérifier le contenu du dossier et l'historique de Git à l'aide des commandes déjà **vues précédemment**
