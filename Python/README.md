# Le langage Python 

## Activité de recherche d'informations

- Quels sont les avantages et les inconvénients du langage Python par rapport aux autres langages de programmation ?

- Qu'est-ce qu'un test unitaire ?

- A quoi correspond la bibliothèque standard en Python ?

- Qu'est-ce qu'un environnement virtuel ?

- Qu'est-ce qu'un gestionnaire de paquets Python ?

- Quelles sont les différences entre pip et conda ?

- Qu'est-ce qu'un environnement de développement (IDE) ? Donner quelques exemples d'IDE

- Qu'est-ce qu'un Jupyter Notebook ?

- Qu'est-ce que le service Colaboratory de Google ?

## Activité bases de statistiques

Dans cette partie, nous allons voir les notions de base de statistiques pour une analyse de données univariée. L'activité est à effectuer dans un Jupyter Notebook **sans import de bibliothèques externes** dans Colaboratory (pas d'importation de Numpy, Pandas...)

- Copier le fichier "Bases_statistiques.ipynb" dans votre drive Google

- Ouvrir le fichier dans Colaboratory (clic droit, Ouvrir avec, Google Colaboratory)

- Répondre à toutes les questions des activités du notebook

- Envoyer votre notebook rempli sur votre branche du dépôt Gitlab

## Activité d'algorithmique

La plateforme [CondinGame](https://www.codingame.com/start) permet de s'entrainer à coder sur différents types de problèmes d'algorithmique.

- Résoudre un des 3 problèmes d'algorithmique suivant :

    - Niveau simple (notions de base de Python) : https://www.codingame.com/training/easy/defibrillators

    - Niveau moyen (distance L1) : https://www.codingame.com/training/medium/network-cabling

    - Niveau difficile (arbre de décision): https://www.codingame.com/training/hard/basic-decision-tree---1

- Envoyer votre code dans un fichier Python (.py) avec la référence du problème sur votre branche du dépôt Gitlab


