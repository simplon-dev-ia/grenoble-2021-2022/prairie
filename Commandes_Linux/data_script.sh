#!/bin/bash

# Ce script télécharge un fichier distant via l'URL passée en argument
# et le stocke dans un fichier de destination spécifié.
#
# Utilisation:
#   ./data_script.sh <url> <fichier-destination>

# URL du fichier à télécharger
FILE_URL=$1

# Fichier de destination
OUTPUT_FILE=$2

echo "Téléchargement du fichier distant ${FILE_URL} dans le fichier ${OUTPUT_FILE} en cours..."

##############################################################################

# TODO : télécharger le fichier distant `FILE_URL` dans le fichier `OUTPUT_FILE`

##############################################################################

echo "Téléchargement terminé !"
