# Le langage Python en data science

## Activité de recherche d'informations

- Lister des bibliothèques externes de Python utilisées pour manipuler des données

- Lister des bibliothèques externes de Python utilisées pour visualiser des données

- Lister des bibliothèques externes de Python utilisées pour entrainer des modèles de machines learning

- Lister des bibliothèques externes de Python utilisées pour entrainer des modèles de deep learning

## Activité bases de statistiques - Partie 2

Dans cette partie, nous allons voir les notions de base de statistiques avec les librairies de datascience en Python.

- Copier le fichier "Bases_statistiques_2.ipynb" dans votre drive Google

- Ouvrir le fichier dans Colaboratory (clic droit, Ouvrir avec, Google Colaboratory)

- Répondre à toutes les questions des activités du notebook

- Envoyer votre notebook rempli sur votre branche du dépôt Gitlab