# Initiation Data / IA avec Python

En guise de dernière activité de prairie, nous vous proposons de reitérer le
mini-projet de prédiction de valeur foncière immobilière, mais cette fois-ci
nous utiliserons l'eco-système Python au lieu de Google Sheets.

Activité précédente avec Google Sheets : [Data IA Spreadsheet](../data_ia_spreadsheet/README.md)

## Objectif

L'application finale se présentera sous la forme d'un Jupyter Notebook présentant
un petit calculateur interactif, permettant à l'utilisateur de prédire le prix
d'un bien immobilier selon les critères demandés.

Pour les différentes étapes possibles, se référer à l'activité précédente avec Google Sheets.

## Mise en place

Cette activité doit être réalisée dans un Jupyter Notebook. Vous pouvez utiliser
au choix :

- [Google Colab](https://colab.research.google.com)
- Un environnement virtuel local Python avec les bibliothèque Data Science
  installées (Jupyter, Pandas, Matplotlib, etc.)

Vous pouvez utiliser la bibliothèque de votre choix afin d'effectuer une régression linéaire
(`numpy`, `scipy`, `statsmodels`, `scikit-learn`, etc).

Dans tous les cas, vous devez commiter et pousser votre Notebook rempli sur
votre branche de ce dépôt Gitlab.

## Restitution

Restitution d'activité orale de **5 minutes maximum**, avec support de présentation
type diaporama de **3 slides minimum**.
