# Publication de notebooks

> Comment déployer et mettre à dispositions ses notebooks en ligne ?

## Recherche d'information

- [ ] Qu'est-ce qu'un _site-web statique_ ?
- [ ] Dans quels formats peuvent être exportés les Jupyter Notebook ?
- [ ] Quels sont les générateurs de site-web statiques en Python ?
- [ ] Qu'est-ce que GitLab Pages ?

## Mise en pratique

Nous allons utiliser le programme [MkDocs](https://www.mkdocs.org) afin de mettre
à disposition nos notebooks Jupyter. **Ce n'est pas la seule solution mais elle
a le mérite d'être simple**: ce système utilise nativement le format Markdown
pour rédiger des articles, mais une extension permet aussi d'utiliser les notebooks.

### Création du site-web statique avec MkDocs

1. Créer un nouveau dossier en dehors de la prairie, par exemple `mes-notebooks`

2. Créer un environnement virtuel Python avec l'outil de votre choix (`pipenv`, `poetry`, `venv`).
   Par exemple avec `venv`:
```bash
cd mes-notebooks
python3 -m venv venv
source venv/bin/activate
```

3. Installer les packages suivants (soit avec `pip` directement, soit avec `pipenv` ou `poetry`):

    - `mkdocs`
    - `mkdocs-material`
    - `mknotebooks`

4. Créer un dossier de contenu nommé `content`

5. Dans ce dossier, ajouter un fichier Markdown nommé `index.md` avec le contenu suivant :
```md
# Home

Welcome to my personal notebooks!
```

6. Dans ce dossier, ajouter un notebook Jupyter de votre choix, idéalement contenant des visualisations et des _Dataframes_

7. Ajouter le fichier de configuration Mkdocs nommé `mkdocs.yml` avec le contenu minimal suivant:
```yaml
site_name: My Personal Notebooks
site_description: Experimenting with AI using Jupyter Notebooks
site_author: My Name
site_url: https://username.gitlab.io/repo-name/

docs_dir: content
site_dir: build

extra:
  version: 1.0.0

theme:
  name: material
  language: en
  font: false
  features:
    - navigation.sections
    - navigation.top
    - search.highlight

plugins:
  - search:
      lang:
        - en
  - mknotebooks

markdown_extensions:
  - pymdownx.highlight
  - pymdownx.superfences
```

8. Tester la configuration en lançant le serveur web de développement en local :
```bash
mkdocs serve
```

9. Tester la construction du site-web en local en inspectant le dossier `build` à posteriori :
```bash
mkdocs build
```

10. Si utilisation de `pip` au lieu de `pipenv` ou `poetry`, générer un fichier de dépendances :
```bash
pip freeze > requirements.txt
```

### Déploiement avec GitLab Pages

Nous allons utiliser le service d'hébergement de site-web statique GitLab Pages,
qui a le mérite d'être pré-intégré à GitLab et entièrement gratuit. Afin de
pouvoir utiliser ce service, nous devons configurer un _pipeline de déploiement continu_
en utilisant le service GitLab-CI.

1. Créer un dépôt Git sur GitLab (si ce n'est pas déjà le cas)

2. Modifier la variable `site_url` du fichier `mkdocs.yml` avec votre nom d'utilisateur GitLab
   et le nom de code de votre dépôt Git

3. Commiter et pousser les fichiers suivants :

    - `mkdocs.yml`
    - `content/*`
    - Si utilisation de `pip`: `requirements.txt`
    - Si utilisation de `pipenv`: `Pipfile`, `Pipfile.lock`

4. Ajouter, commiter et pousser le fichier de configuration GitLab-CI nommé `.gitlab-ci.yml` avec le contenu suivant :
```yml
stages:
  - build
  - deploy

docs:
  image: python:3.9
  stage: build
  script:
    # si utilisation de pip decommenter ces lignes :
    # - python3 -m venv .
    # - source bin/activate
    # - pip install -r requirements.txt
    # - mkdocs build
    # si utilisation de pipenv decommenter ces lignes :
    # - pip install pipenv
    # - pipenv install --dev --deploy
    # - pipenv run mkdocs build --strict
  artifacts:
    name: "docs-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
    paths:
      - build

pages:
  stage: deploy
  only:
    - main
  script:
    - cp -R build public
  artifacts:
    paths:
      - public
```

5. Vérifier le bon déroulement du pipeline de déploiement continu dans la section "CI/CD"
   et corriger les erreurs si besoin

6. Accéder au site-web statique déployé via son URL GitLab du type : `https://username.gitlab.io/repo-name/`
   (cela peut prendre quelques minutes)

## Story telling

Maintenant que vous avez un moyen de mettre à disposition des notebooks en ligne
(ainsi que des fichiers Markdown ordinaires), vous pouvez commencer à penser à
la narration de vos notebooks !

En effet, il ne suffit pas simplement d'enchainer les cellules de code et d'afficher
les résultats : pensez à comment se construit un **article** ou un **tutoriel**,
avec son **contexte**, ses **problématiques**, ses **contraintes**, ses **objectifs**
et ses **conclusions**.

Ce procédé s'appelle le **storytelling**. Vous pouvez vous inspirer sur le fond des
exemples de la plateforme [Observable](https://observablehq.com).

### Mise en pratique

Pour vous entraîner :

- [ ] Reprenez une expérience / un tutoriel / un exercice que vous
avez trouvé intéressant
- [ ] Racontez une histoire courte avec le contenu
- [ ] Utilisez les sections Markdown pour organiser votre discours
- [ ] Publiez votre notebook en ligne !
