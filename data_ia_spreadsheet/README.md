# Initiation Data / IA avec un tableur

Durant la prairie, nous vous proposons ce premier mini-projet d'une durée de 2 jours
permettant de mettre en douceur un premier pied pratique dans le monde des données
et de l'intelligence artificielle.

Pour des raisons de praticité, nous allons utiliser le service gratuit **Google Sheets**
inclus avec tous les comptes Google.

![Google Sheets Logo](google-sheets.svg)

## Objectif

Vous travaillez pour une agence immobilière Grenobloise qui souhaite fournir à
ses clients un simulateur de prix le plus fiable possible. La dirigeante de la
société a entendu dire que le gouvernement vient récemment de publier en données
ouvertes les "demandes de valeurs foncières" du premier semestre 2021. Elle souhaite
pouvoir mettre à profit ces données pour développer une application intelligente.

Votre mission est de trouver, analyser et exploiter ces données afin de trouver
une méthode permettant de prédire les prix des biens immobiliers en fonction
d'un ou plusieurs critères...

... avec Google Sheets comme seul outil de manipulation de données à votre disposition !

## Modalités

L'objectif pédagogique de ce premier (petit) projet est de s'initier au cycle de vie d'un projet Data et IA :

- Comprendre le contexte, les objectifs et les contraintes du projet
- Rechercher de l'information pertinente
- Extraire, analyser et transformer des données
- Exploiter des données
- Avoir recours à l'intelligence artificielle si nécessaire

À l'issue du projet, vous devrez effectuer une restitution de vos résultats en
5 minutes maximum, avec le support de votre choix.

Cette activité est **à réaliser individuellement**, mais **en réfléchissant en binôme** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]` avec ses propres observations et remarques

## Préliminaire : Recherche d'information

Avant de commencer l'activité, vous devez effectuer une recherche d'information sur les questions suivantes :

- [ ] Quel type de jeu de donnée est stocké dans un tableau ?
- [ ] Quelles sont les deux grands types de données statistiques ?
- [ ] Quel(s) type(s) de problème(s) est(sont) généralement associé(s) aux données immobilières ?

### Observations

`[A COMPLETER]`

## Partie 1 : Extraction des données

Le gouvernement français met à disposition en _open data_ (_données ouvertes_)
les "demande de valeurs foncières" année par année sur la plateforme Etalab:

- https://www.data.gouv.fr/fr/datasets/demandes-de-valeurs-foncieres/
- https://www.data.gouv.fr/en/datasets/demandes-de-valeurs-foncieres-geolocalisees/

Marche à suivre :

- [ ] Trouver le bon jeu de données des DVF 2021 pour le département de l'Isère
- [ ] Extraire le jeu de données
- [ ] Ouvrir et observer le jeu de données dans un éditeur de texte

### Observations

`[A COMPLETER]`

## Partie 2 : Chargement des données

Chargons les données afin de commencer à les analyser :

- [ ] Créer un tableur Google Sheets
- [ ] Renommer la feuille initiale en "SOURCE" et ajouter les méta-données de la source de données utilisée
- [ ] Importer fichier CSV en créant une nouvelle feuille
- [ ] Renommer la feuille importée en "DONNEES BRUTES"
- [ ] Utiliser le tableau en mode "filtre" pour explorer les données
- [ ] Afficher les statistiques des colonnes (total, cellules vides, valeurs uniques, doublons, etc.)

### Observations

`[A COMPLETER]`

## Partie 3 : Analyse préliminaire

Il est généralement une bonne idée d'analyser les données brutes afin d'évaluer les possibilités :

- [ ] Identifier la donnée représentant l'objectif : la valeur foncière
- [ ] Identifier les données représentant des caractéristiques intéressantes : le type de local, la surface en mètre carré, le nombre de pièces principales
- [ ] Créer un graphique d'analyse entre la surface en mètre carré et la valeur foncière
- [ ] Créer un graphique d'analyse entre le nombre de pièces principales et la valeur foncière
- [ ] Créer une table pivot permettant d'obtenir la valeur foncière moyenne par type de local
- [ ] Insérer un graphique à partir de la table pivot

**Bonus** : tenter d'identifier si d'autres données représentent des caractéristiques intéressantes,
et si elles sont manquantes comment les obtenir ?

### Observations

`[A COMPLETER]`

## Partie 4 : Nettoyage des données

**Note** : avant et après chaque transformation, pensez à afficher les statistiques
des colonnes afin d'en valider l'effet !

Afin de pouvoir exploiter les données dans les meilleurs conditions possibles,
il convient de nettoyer et/ou d'enrichir les données :

- [ ] Dupliquer la feuille "DONNEES BRUTES" en "DONNEES PROPRES" et travailler sur cette dernière
- [ ] Utiliser le tableau en mode "filtre"
- [ ] Fixer les intitulés des colonnes sur la première ligne
- [ ] Supprimer les colonnes non intéressantes (identifiants spécifiques, redondances, etc.)
- [ ] Formatter les nombres
- [ ] Formatter les dates
- [ ] Supprimer les espaces inutiles
- [ ] Supprimer les doublons
- [ ] Si nécessaire, tenter de trouver une stratégie pour remplir les données manquantes

### Observations

`[A COMPLETER]`

## Partie 5 : Exploitation des données

Il est temps d'exploiter pleinement les données nettoyées et d'expérimenter un modèle de prédiction linéaire.

Pour chaque données de caractéristique identifiée :

- [ ] Visualiser la relation avec la donnée cible
- [ ] Anticiper le modèle de relation en affichant la ligne de tendance
- [ ] Trouver la fonction Google Sheet pour calculer une _régression linéaire_
- [ ] Calculer la régression linéaire entre les 2 données (variable explicative et variable cible)
- [ ] Afficher la droite avec les coefficients trouvés
- [ ] Valider la véracité du modèle en calculant le _coefficient de détermination (R2)_

À la fin du processus, classer et conserver les modèles suivant le score R2.

**Bonus** : effectuer le même processus en comparant cette fois plusieurs variables explicatives contre la variable cible.

### Observations

`[A COMPLETER]`

## Partie 6 : Déploiement

Vous avez obtenu des coefficients modélisant la relation entre une ou plusieurs
variables explicatives et la variable cible. Il est temps de délivrer un prototype
d'application à votre cheffe :

- [ ] Schématiser visuellement un formulaire d'application très simple
- [ ] Créer une nouvelle feuille nommée "APPLICATION"
- [ ] Créer les colonnes correspondants aux caractéristiques à remplir par l'utilisateur
- [ ] Créer la colonne de prédiction avec une formule utilisant le modèle
- [ ] Tester le prototype avec plusieurs jeux de données d'entrée

### Observations

`[A COMPLETER]`

## Conclusion

En binôme, préparer une restitution d'activité de 5 minutes maximum.

Quelques axes de réflexion possibles :

- Qu'avez-vous appris à l'issue de cette activité ?
- Quels sont les avantages d'un tableur ?
- Quels sont les inconvénients d'un tableur ?
- Quels sont les limites d'un tableur ?

### Observations

`[A COMPLETER]`
