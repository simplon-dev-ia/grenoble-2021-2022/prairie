# Activité de veille technologique sur le workflow d'intelligence artificielle

En binôme, vous allez analyser un article sur un sujet lié à l'intelligence artificielle.

---

- **Sujet N°1 : Hyperspeed EDA: The Next Data Analytics Frontier**

[Article en anglais](https://www.forbes.com/sites/forbestechcouncil/2021/08/04/hyperspeed-eda-the-next-data-analytics-frontier/)

- **Sujet N°2 : Le feature engineering doit impliquer les experts métiers!**

[Article en français](https://www.usinenouvelle.com/blogs/sylvain-duranton/le-feature-engineering-doit-impliquer-les-experts-metiers.N767814)

- **Sujet N°3 : Five Factors For Effective Execution Of Data Science Models**

[Article en anglais](https://www.forbes.com/sites/forbestechcouncil/2021/11/23/five-factors-for-effective-execution-of-data-science-models/)

- **Sujet N°4 : The Onset Of Data-Centric AI And Why It’s Here To Stay**

[Article en anglais](https://www.forbes.com/sites/forbestechcouncil/2021/12/03/the-onset-of-data-centric-ai-and-why-its-here-to-stay/)

- **Sujet N°5 : Amazon announces new machine learning products at re:Invent**

[Article en anglais](https://bdtechtalks.com/2021/12/02/amazon-aws-reinvent-machine-learning-products/)

- **Sujet N°6 : AWS launches new SageMaker features to make scaling machine learning easier**

[Article en anglais](https://techcrunch.com/2021/12/01/aws-launches-new-sagemaker-features-to-make-scaling-machine-learning-easier/)


- **Sujet N°7 : MLOps, l’alchimie de tous les métiers de l’intelligence artificielle ?**

[Article en français](https://www.silicon.fr/avis-expert/mlops-lalchimie-de-tous-les-metiers-de-lintelligence-artificielle)

---

## TODO : synthèse de l'article

- Préparer une présentation de 2 ou 3 minutes sur les idées principales contenues dans l'article étudié.
- Extraire les mots-clés liés aux données présents dans l'article (de 3 à 5 mots-clés)
- Suite à la présentation des différents articles, créer une liste des étapes d'un projet d'intelligence artificielle (de A à Z)

